const apiUrl = 'https://backed-users-demo.herokuapp.com/api/';
export const environment = {
  production: false,
  login: apiUrl + 'login',
  users: apiUrl + 'users'
};
