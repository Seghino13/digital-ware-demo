import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LayoutPrivateComponent } from './themes/private/layout-private/layout-private.component';
import { LayoutPublicComponent } from './themes/public/layout-public/layout-public.component';

const routes: Routes = [
  {
    path:'',
    component: LayoutPublicComponent,
    loadChildren: ()=> import('./pages/public/public-pages/public-pages.module').then( m => m.PublicPagesModule),
  },
  {
    path:'dashboard',
    component: LayoutPrivateComponent,
    loadChildren: ()=> import('./pages/private/dashborad/dashborad.module').then( m => m.DashboradModule),
    canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
