import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { DbStorageService } from '../services/helpers/db-storage.service';
import { SessionService } from '../services/helpers/session.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private _db:DbStorageService, private router: Router, private _session: SessionService){

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const token = this._db.getToken();
      token.then(t =>{
        const session:any = this._session.getSession();
        if( session.message?.startsWith('Invalid token specified')){
          sessionStorage.clear();
          this.router.navigate(['/']);
        }
      }).catch( e=>{
        console.log( e );
        sessionStorage.clear();
        this.router.navigate(['/']);
      })
      
    return true;
  }
  
}
