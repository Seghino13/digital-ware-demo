export interface ISession{
    active: true,
    created_at: Date,
    email: string,
    name: string,
    role: string,
    uid: string
}