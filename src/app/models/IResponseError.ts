export interface IResponseError {
    ok: boolean,
    msg: string
}