export interface IUserNew{
    name: string,
    password: string,
    email:string,
    uid?: string,
    role?: string
}