import { ISession } from "./ISession"

export interface IResponseUsers{
    response: boolean,
    Users?: ISession[]
    uid: string,
    users?: ISession
}

