export interface IResponseLogin{
    ok: boolean,
    token: string
}