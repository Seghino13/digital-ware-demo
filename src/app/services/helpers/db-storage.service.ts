import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DbStorageService {

  constructor() { }

  saveToken( token: string){
    return new Promise( (res, rej)=>{
      try{
        const save = sessionStorage.setItem('token', token);
        res(true);
      }catch(e){
        console.log('@Error save to storage=>', e);
        rej(false);
      }
    })
  }

  getToken(){
    return new Promise( (res, rej)=>{
      try{
        const token = sessionStorage.getItem('token');
        res(token);
      }catch(e){
        console.log('@Error get to storage=>', e);
        rej( new Error( JSON.stringify(e) ) );
      }
    })
  }
}
