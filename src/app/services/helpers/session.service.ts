import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { ISession } from 'src/app/models/ISession';
@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private router:Router) { }

  getSession = ()=>{
    try{
      const token = String(sessionStorage.getItem('token'));
      const decode:any = jwt_decode(token);
      if(decode){
        
        try{
          const session: ISession = {
            active: decode.data.active,
            created_at: decode.data.created_at,
            email: decode.data.email,
            name: decode.data.name,
            role: decode.data.role,
            uid: decode.data.uuid,
          }
          return session
        }catch(e){
          console.log('@Error  mapping session', e);
          this.logOut();
        }
      }
      return 
    }catch (e){
      return e
    }
  }

  logOut(){
    sessionStorage.clear();
    this.router.navigate(['/']);
  }
}
