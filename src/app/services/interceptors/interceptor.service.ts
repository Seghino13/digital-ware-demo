import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent }
  from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class InterceptorServices implements HttpInterceptor {
    intercept(req : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>> {
      const token: string = String(sessionStorage.getItem('token'));

      const request = req.clone({
        setHeaders:{
          authorization: `${ token }`
        }
      });
      return next.handle(request);
    }
}