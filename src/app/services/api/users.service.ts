import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IResponseUsers } from 'src/app/models/IResponseUsers';
import { IUserNew } from 'src/app/models/IUserNew';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers = () =>{
    return this.http.get<IResponseUsers>(environment.users);
  }

  createUsers = ( user: IUserNew ) =>{
    return this.http.post<IResponseUsers>(environment.users, user);
  }

  updateUsers = ( user: IUserNew, uuid: string ) =>{
    console.log(user);
    return this.http.put<IResponseUsers>(environment.users + '/' + uuid, user);
  }

  deleteUsers = ( uuid: string ) =>{
    return this.http.delete<IResponseUsers>(environment.users + '/' + uuid);
  }
}
