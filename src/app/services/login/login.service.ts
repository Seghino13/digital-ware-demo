import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IResponseLogin } from 'src/app/models/IResponseLogin';
import { ILogin } from 'src/app/models/ILogin';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(private http: HttpClient) { }

  login = ( login: ILogin ) =>{
    return this.http.post<IResponseLogin>( environment.login, login);
  }
}
