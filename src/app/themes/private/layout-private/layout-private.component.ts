import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/helpers/session.service';

@Component({
  selector: 'app-layout-private',
  templateUrl: './layout-private.component.html',
  styleUrls: ['./layout-private.component.scss'],
  providers: [SessionService]
})
export class LayoutPrivateComponent implements OnInit {

  constructor(private _session: SessionService) { }

  ngOnInit() {
    
  }

}
