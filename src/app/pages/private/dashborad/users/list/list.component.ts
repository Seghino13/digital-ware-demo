import { Component, OnInit } from '@angular/core';
import { IResponseUsers } from 'src/app/models/IResponseUsers';
import { ISession } from 'src/app/models/ISession';
import { UsersService } from 'src/app/services/api/users.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [UsersService]
})
export class ListComponent implements OnInit {
  users: ISession[] = [];
  constructor(
          private _userService: UsersService, 
          private modalService: NgbModal,
          private router: Router) { }
  userDelete?: ISession;
  ngOnInit() {
    this.getUsers();
  }

  async getUsers(){
    this.users = [];
    try{
      const request:any = await this._userService.getUsers().toPromise();
      for(const i in request.users){
        this.users.push(
          {
            active: request.users[i].active,
            created_at: request.users[i].created_at,
            email: request.users[i].email,
            name: request.users[i].name,
            role: request.users[i].role,
            uid: request.users[i].uuid,
          }
        );
      }

      this.users = this.users.filter( user => user.active );
    }catch(e){
      console.log('@Error get users', e);
    }
  }

  open(content:any, user: ISession) {
    this.userDelete = user;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
       
    }, (reason) => {
      this.userDelete = undefined;
    });
  }

  async deleteUser(){
  
    try{
      const request:any = await this._userService.deleteUsers(String(this.userDelete?.uid)).toPromise();
      console.log(request);
      if(request){
        this.modalService.dismissAll();
        this.getUsers();
      }
    }catch(e){
      console.log('@Error delete users', e);
    }
  }

  editUser(user: ISession){
 
   // this.router.navigate(['/dashboard/edit']);
  }

}
