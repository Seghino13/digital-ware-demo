import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ISession } from 'src/app/models/ISession';
import { IUserNew } from 'src/app/models/IUserNew';
import { UsersService } from 'src/app/services/api/users.service';
import { SessionService } from 'src/app/services/helpers/session.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [UsersService, SessionService]
})
export class CreateComponent implements OnInit {


  formData =  new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    role: new FormControl('', [Validators.required]),
  });
  edit:string = '';
  userSubscription?: Subscription;

  user?:ISession;
  
  constructor(private _usersService: UsersService, private router:Router, public route: ActivatedRoute, private _session: SessionService) { 
   
    const params = this.router.getCurrentNavigation()?.extras.state;
    console.log(params);
    if( params ){
      this.formData.get('email')?.setValue(params.email);
      this.formData.get('password')?.setValue('');
      this.formData.get('name')?.setValue(params.name);
      this.formData.get('role')?.setValue(params.role);
      this.edit = params.uid;
    }
  }

  ngOnInit() {
    this.getSessionData();
  }

  getSessionData(){
    const user:any = this._session.getSession();
    this.user = user;
    console.log(this.user);
  }

  


  get f(): { [key: string]: AbstractControl } {
    return this.formData.controls;
  }

  async send(){
    
    if( !this.formData.valid ) return;
    
    const user : IUserNew = this.formData.value;

    try{
      if( this.edit ){
        const request = await this._usersService.updateUsers(user, this.edit ).toPromise();

        if(request){
          this.router.navigate(['/dashboard']);
        }
      }else{
        const request = await this._usersService.createUsers(user).toPromise();
        if(request){
          this.router.navigate(['/dashboard']);
        }
      }
      
      
        
    }catch(e){
      console.log('@e Login =>',e);
    }

  }

}
