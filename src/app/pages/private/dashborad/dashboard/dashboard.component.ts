import { Component, OnInit } from '@angular/core';
import { ISession } from 'src/app/models/ISession';
import { SessionService } from 'src/app/services/helpers/session.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [SessionService]
})
export class DashboardComponent implements OnInit {
  user?:ISession;
  constructor(private _session: SessionService) { }

  ngOnInit() {
    this.getSessionData();
  }

  getSessionData(){
    const user:any = this._session.getSession();
    this.user = user;
  }

}
