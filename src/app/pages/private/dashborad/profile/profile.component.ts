import { Component, Input, OnInit } from '@angular/core';
import { ISession } from 'src/app/models/ISession';
import { SessionService } from 'src/app/services/helpers/session.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [SessionService]
})
export class ProfileComponent implements OnInit {

  @Input() session?: ISession;
  constructor(private _sessionService: SessionService) {
    
  }

  ngOnInit(): void {
  }

  logout(){
    this._sessionService.logOut();
  }
}
