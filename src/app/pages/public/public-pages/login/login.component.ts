import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ILogin } from 'src/app/models/ILogin';
import { IResponseError } from 'src/app/models/IResponseError';
import { DbStorageService } from 'src/app/services/helpers/db-storage.service';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService, DbStorageService]
})
export class LoginComponent implements OnInit {

  formData =  new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  })
  error?:any;
  constructor(private _loginServices: LoginService, private router:Router, private _db:DbStorageService) { }

  ngOnInit() {
  }

  get f(): { [key: string]: AbstractControl } {
    return this.formData.controls;
  }

  async send(){
    this.error = undefined;
    if( !this.formData.valid ) return;
    
    const login : ILogin = this.formData.value;
    try{
      const request = await this._loginServices.login(login).toPromise();

      const saveToken = await this._db.saveToken( request.token );

      if( saveToken ){
        this.router.navigate(['/dashboard']);
      }      
    }catch(e){
      console.log('@e Login =>',e);
      this.error = e;
    }
  }

}
